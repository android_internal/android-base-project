package com.madison.client.appname.extention.helper.validation

import android.view.View

interface OnViewDataChangeListener {
    fun onViewDataChange(view: View)
}
